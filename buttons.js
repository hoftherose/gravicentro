function reset() {
  Matter.World.remove(world, polygon.body);
  polygon = null;
  points = [];
}

function undoVertice() {
  if (!polygon){
    if (points != []) {
      points.pop();
    }
  }
  else {
    Matter.World.remove(world, polygon.body);
    polygon = null;
  }
}

function undoLine() {
  if (polygon){
    if (polygon.lines != []) {
      polygon.lines.pop();
    }
  }
}

function undoPoint() {
  if (polygon){
    if (polygon.points[0] != []) {
      polygon.points[0].pop();
      polygon.points[1].pop();
    }
  }
}

function drawFigure(figurePoints) {
  if (!polygon){
    polygon = new Polygon(figurePoints, world);
  }
  else {
    Matter.World.remove(world, polygon.body);
    polygon = null;
    polygon = new Polygon(figurePoints, world);
  }
  points = figurePoints;
}

function drawSquare() {
  let squarePoints = [{x: 100, y: 100},
                      {x: 200, y: 100},
                      {x: 200, y: 200},
                      {x: 100, y: 200}];
  drawFigure(squarePoints);
}

function drawTrapezoid() {
  let trapezoidPoints = [{x: 200, y: 100},
                         {x: 300, y: 100},
                         {x: 400, y: 200},
                         {x: 100, y: 200}];
  drawFigure(trapezoidPoints);
}
