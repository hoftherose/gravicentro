function makeMouseConstraint(canvas){
  const mouse = Matter.Mouse.create(canvas.elt);
  const options = {
    mouse: mouse,
    constraint: {
      stiffness: 0.1,
      angularStiffness: 0.001
    }
  }
  mConstraint = Matter.MouseConstraint.create(engine, options);
  Matter.World.add(world, mConstraint);
}

function createPolygon(points){
  let newpoint;
  if (clicked && mouseX < canvasX && mouseY < canvasY){
    newpoint = {x: mouseX, y: mouseY};
    if (points.length == 0 || (points.length > 0 && calcDist(points[points.length-1], newpoint) > THRESH)){
      points[points.length] = newpoint;
    }
  }
  return points;
}

function mousePressed() {
  clicked = true;
}

function mouseReleased() {
  clicked = false;
}
