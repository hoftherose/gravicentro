function moodLight() {
  const speed = 1.1;
  if (clr[0] == 0 && clr[1] != 0) {
    clr[1] = Math.max(clr[1] - speed, 0);
    clr[2] = Math.min(clr[2] + speed, 255);
  }
  if (clr[1] == 0 && clr[2] != 0) {
    clr[2] = Math.max(clr[2] - speed, 0);
    clr[0] = Math.min(clr[0] + speed, 255);
  }
  if (clr[2] == 0 && clr[0] != 0) {
    clr[0] = Math.max(clr[0] - speed, 0);
    clr[1] = Math.min(clr[1] + speed, 255);
  }
}
