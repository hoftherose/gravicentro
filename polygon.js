class Polygon{
  constructor(points, world) {
    this.points = points;
    this.body = Matter.Bodies.fromVertices(points[0]["x"], points[0]["y"], points);
    Matter.World.add(world, this.body);
    this.lines = [];
    this.points = [[],[]];
  }
  show (){
    beginShape();
    for (let p of this.body.vertices) {
      vertex(p["x"], p["y"]);
    }
    endShape(CLOSE);
  }
}

function polygonFactory(){
  points = createPolygon(points);
  if (points.length >= 2 && calcDist(points[0],points[points.length-1]) < THRESH){
    points = points.slice(0,points.length-1);
    if (points.length == 2){
      points = []
    } else {
      polygon = new Polygon(points, world);
    }
  }
  else {
    plot_connections(points);
  }
}
