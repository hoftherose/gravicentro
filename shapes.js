class Shapes {
  constructor (x,y,w,h){
    this.body = Matter.Bodies.rectangle(x+w/2,y+h/2,w,h);
    Matter.World.add(world, this.body);
    this.w = w;
    this.h = h;
  }
  show() {
    const pos = this.body.position;
    const angle = this.body.angle;
    push();
    translate(pos.x, pos.y);
    rotate(angle);
    fill(255);
    rect(-this.w/2,-this.h/2,this.w, this.h);
    pop();
  }
}

class Walls extends Shapes {
  constructor (x,y,w,h) {
    super(x,y,w,h)
    this.body.isStatic = true;
  }
}

function makeWalls(){
  let w = [];
  w[0] = new Walls(-18,-18,width+20,20);
  w[1] = new Walls(width-2,-18,20,height+20);
  w[2] = new Walls(2,height-2,width+20,20);
  w[3] = new Walls(-18,2,20,height+20);
  return w;
}
