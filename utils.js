function calcDist(pointA, pointB) {
  let distance = pow(pointA["x"]-pointB["x"], 2) + pow(pointA["y"]-pointB["y"], 2);
  return distance;
}

function plot_connections(points){
  stroke('white');
  for (let i = 0; i < points.length-1; i++){
    line(points[i]["x"],points[i]["y"],points[i+1]["x"],points[i+1]["y"]);
  }
  strokeWeight(5);
  stroke('red');
  for (let i = 0; i < points.length; i++){
    point(points[i]["x"],points[i]["y"]);
  }
  strokeWeight(1);
  stroke('black');
}

function findLine() {
  let intersections = [];
  let vert = polygon.body.vertices;
  for (let i = 0; i < vert.length; i++){
    let p1 = vert[i];
    let p2 = vert[(i+1)%vert.length];
    if ((p1["x"] < mouseX && mouseX < p2["x"]) || (p1["x"] > mouseX && mouseX > p2["x"])){
      if (abs(p1["x"] - p2["x"]) != 0){
        p = (p1["x"] - mouseX)/(p1["x"] - p2["x"]);
      }
      intersections[intersections.length] = [p,i];
    }
  }
  return intersections;
}

function addLine() {
  let intersections = findLine();
  if (intersections.length == 2){
    polygon.lines[polygon.lines.length] = intersections;
  }
}

function drawLines() {
  stroke('black');
  for (let l of polygon.lines){
    let p1 = l[0][0]; let i1 = l[0][1]; let p2 = l[1][0]; let i2 = l[1][1];
    let vert = polygon.body.vertices;
    let pointA = [vert[i1]["x"]*(1-p1)+vert[(i1+1)%vert.length]["x"]*p1, vert[i1]["y"]*(1-p1)+vert[(i1+1)%vert.length]["y"]*p1];
    let pointB = [vert[i2]["x"]*(1-p2)+vert[(i2+1)%vert.length]["x"]*p2, vert[i2]["y"]*(1-p2)+vert[(i2+1)%vert.length]["y"]*p2];
    line(pointA[0], pointA[1], pointB[0], pointB[1]);
  }
  stroke('black');
}

function addPoint() {
  if (!polygon){
    return 0;
  }
  let intersections = findLine();
  let point = 0;
  if (intersections.length == 2){
    let p1 = intersections[0][0]; let i1 = intersections[0][1]; let p2 = intersections[1][0]; let i2 = intersections[1][1];
    let vert = polygon.body.vertices;
    let pointA = [vert[i1]["x"]*(1-p1)+vert[(i1+1)%vert.length]["x"]*p1, vert[i1]["y"]*(1-p1)+vert[(i1+1)%vert.length]["y"]*p1];
    let pointB = [vert[i2]["x"]*(1-p2)+vert[(i2+1)%vert.length]["x"]*p2, vert[i2]["y"]*(1-p2)+vert[(i2+1)%vert.length]["y"]*p2];
    let segment = [pointA, pointB];
    if ((Math.max(segment[0][1], segment[1][1], mouseY) != mouseY) && (Math.min(segment[0][1], segment[1][1], mouseY) != mouseY)){
      polygon.points[0][polygon.points[0].length] = intersections;
      polygon.points[1][polygon.points[1].length] = (segment[0][1]-mouseY)/(segment[0][1]-segment[1][1]);
    }
  }
}

function drawPoints() {
  for (let i = 0; i < polygon.points[0].length; i++){
    let l = polygon.points[0][i];
    let p1 = l[0][0]; let i1 = l[0][1]; let p2 = l[1][0]; let i2 = l[1][1];
    let p = polygon.points[1][i];
    let vert = polygon.body.vertices;
    let pointA = [vert[i1]["x"]*(1-p1)+vert[(i1+1)%vert.length]["x"]*p1, vert[i1]["y"]*(1-p1)+vert[(i1+1)%vert.length]["y"]*p1];
    let pointB = [vert[i2]["x"]*(1-p2)+vert[(i2+1)%vert.length]["x"]*p2, vert[i2]["y"]*(1-p2)+vert[(i2+1)%vert.length]["y"]*p2];
    let pointC = [pointB[0]*(p)+pointA[0]*(1-p), pointB[1]*p+pointA[1]*(1-p)];
    strokeWeight(5);
    point(pointC[0], pointC[1]);
    strokeWeight(1);
  }
}
