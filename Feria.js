let world, engine, shape, walls, polygon;
let points = [];
let canvasX = 1656, canvasY = 920;
let clicked = false;
let img;
let clr = [255,0,0];
const THRESH = 900;

function preload() {
  img = loadImage('assets/Logo.png');
}

function setup() {
  let canvas = createCanvas(canvasX, canvasY);
  engine = Matter.Engine.create();
  world = engine.world;
  walls = makeWalls();
  makeMouseConstraint(canvas);
}

function draw() {
  background(clr[0], clr[1], clr[2]);
  moodLight();
  background(img);
  if (!polygon){
    polygonFactory();
  }
  else{
    polygon.show();
    drawLines();
    drawPoints();
    Matter.Engine.update(engine);
  }
  for (let wall of walls) {
    wall.show();
  }
}

function keyTyped() {
  if (key == "l"){
    addLine();
  }
  if (key == "p"){
    addPoint();
  }
}
